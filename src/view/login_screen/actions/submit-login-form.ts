import { login } from "../../../resources/firebase/actions/user-authentication";

export async function submitLoginForm(event: Event) {
    event.preventDefault();
    const loginForm: HTMLFormElement | null =
        document.querySelector("#login-form");
    if (loginForm == null) {
        throw "could not find login form";
    }
    try {
        await login(loginForm.email.value, loginForm.password.value);
    } catch (error) {
        const errorMsg = document.createElement("div");
        loginForm.append(errorMsg);
        errorMsg.innerHTML = String(error);
        console.error(error);
        throw `error: ${error}`;
    }
}
