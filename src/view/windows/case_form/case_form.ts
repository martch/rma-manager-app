import { CaseData } from "../../../resources/case-data";
import { VALUES } from "../../../external_values/external-values";
import { getCaseData } from "../../../resources/firebase/actions/get-case-data";

export async function caseForm(caseId: string) {
    const data: CaseData | null = await getCaseData(caseId);
    if (data === null) {
        throw "missing case data";
    }

    const form = document.createElement("form");
    form.id = "case-form";
    form.method = "dialog";
    form.autocomplete = "off";

    const closeFormButton = document.createElement("button");
    closeFormButton.id = "close-form-btn";
    closeFormButton.type = "submit";
    closeFormButton.innerText = "aizvērt";

    form.append(formInputs(data), closeFormButton);

    const dialog = document.createElement("dialog");
    dialog.id = "case-dialog";
    dialog.append(form);

    return dialog;
}

function formInputs(data: CaseData) {
    const fragment = document.createDocumentFragment();
    fragment.append(
        clientInputFields(data),
        productFieldsets(),
        additionalInputFields()
    );
    return fragment;
}

function clientInputFields(data: CaseData) {
    const fieldset = document.createElement("fieldset");
    fieldset.id = "client-input-fields-fieldset";

    const legend = document.createElement("legend");
    legend.innerText = "klients";

    fieldset.append(legend);

    const clientCompanyInput = document.createElement("input");
    clientCompanyInput.type = "text";
    clientCompanyInput.name = "client-company";
    clientCompanyInput.value = data.client.client_company;

    const clientCompanyInputLbl = document.createElement("label");
    clientCompanyInputLbl.htmlFor = clientCompanyInput.name;
    clientCompanyInputLbl.innerText = "Uzņēmums";

    fieldset.append(clientCompanyInputLbl, clientCompanyInput);

    const clientNameInput = document.createElement("input");
    clientNameInput.type = "text";
    clientNameInput.name = "client-name";
    clientNameInput.value = data.client.client_name;

    const clientNameInputLbl = document.createElement("label");
    clientNameInputLbl.htmlFor = clientNameInput.name;
    clientNameInputLbl.innerText = "Klienta vārds";

    fieldset.append(clientNameInputLbl, clientNameInput);

    const clientPhoneInput = document.createElement("input");
    clientPhoneInput.type = "tel";
    clientPhoneInput.name = "client-phone";
    clientPhoneInput.value = data.client.client_phone;

    const clientPhoneInputLbl = document.createElement("label");
    clientNameInputLbl.htmlFor = clientPhoneInput.name;
    clientNameInputLbl.innerText = "Telefona numurs:";

    fieldset.append(clientPhoneInputLbl, clientPhoneInput);

    const clientEmailInput = document.createElement("input");
    clientEmailInput.type = "email";
    clientEmailInput.name = "client-email";
    clientEmailInput.value = data.client.client_email;

    const clientEmailInputLbl = document.createElement("label");
    clientEmailInputLbl.htmlFor = clientEmailInput.name;
    clientEmailInputLbl.innerText = "Klienta epasts:";

    fieldset.append(clientEmailInputLbl, clientEmailInput);

    const clientOrderId = document.createElement("input");
    clientOrderId.type = "text";
    clientOrderId.name = "client-order-id";
    clientOrderId.value = data.client.client_order_id;

    const clientOrderIdLbl = document.createElement("label");
    clientOrderIdLbl.htmlFor = clientOrderId.name;
    clientOrderIdLbl.innerText = "Pasūtījuma numurs:";

    fieldset.append(clientOrderIdLbl, clientOrderId);

    const clientOrderDate = document.createElement("input");
    clientOrderDate.type = "date";
    clientOrderDate.name = "client-order-date";
    const date = new Date(data.client.client_order_date);
    clientOrderDate.value = `${date.getFullYear()}-${date.getMonth()}-${date.getDay()}`;

    const clientOrderDateLbl = document.createElement("label");
    clientOrderDateLbl.htmlFor = clientOrderId.name;
    clientOrderDateLbl.innerText = "Pasūtījuma datums:";

    fieldset.append(clientOrderDateLbl, clientOrderDate);

    const clientBankAccount = document.createElement("input");
    clientBankAccount.type = "text";
    clientBankAccount.name = "client-bank-account";
    clientBankAccount.value = data.client.client_bank_account;

    const clientBankAccountLbl = document.createElement("label");
    clientBankAccountLbl.htmlFor = clientBankAccount.name;
    clientBankAccountLbl.innerText = "Bankas konta numurs:";

    fieldset.append(clientBankAccountLbl, clientBankAccount);

    return fieldset;
}

function productFieldsets() {
    const fieldset = document.createElement("fieldset");
    fieldset.id = "case-form-products";
    return fieldset;
}

export function productInputFields() {
    const productCounter = document.querySelector(
        "#new-case-form-product-counter"
    );

    if (productCounter == null) {
        throw "missing product counter";
    }

    let productCount: number = Number(
        productCounter.getAttribute("data-number-of-products")
    );

    const fieldset = document.createElement("fieldset");

    const legend = document.createElement("legend");
    legend.innerText = `prece-${productCount}`;

    fieldset.append(legend);

    const productNameInput = document.createElement("textarea");
    productNameInput.name = `product-name-${productCount}`;

    const productNameInputLbl = document.createElement("label");
    productNameInputLbl.htmlFor = productNameInput.name;
    productNameInputLbl.innerText = "Preces nosaukums:";

    fieldset.append(productNameInputLbl, productNameInput);

    const productSNInput = document.createElement("input");
    productSNInput.type = "text";
    productSNInput.name = `product-sn-${productCount}`;

    const productSNInputLbl = document.createElement("label");
    productSNInputLbl.htmlFor = productSNInput.name;
    productSNInputLbl.innerText = "Sērijas numurs:";

    fieldset.append(productSNInputLbl, productSNInput);

    const productSetInput = document.createElement("input");
    productSetInput.type = "text";
    productSetInput.name = `product-set-${productCount}`;

    const productSetInputLbl = document.createElement("label");
    productSetInputLbl.htmlFor = productSetInput.name;
    productSetInputLbl.innerText = "Komplektācija:";

    fieldset.append(productSetInputLbl, productSetInput);

    const productConditionInput = document.createElement("textarea");
    productConditionInput.name = `product-condition-${productCount}`;

    const productConditionInputLbl = document.createElement("label");
    productConditionInputLbl.htmlFor = productConditionInput.name;
    productConditionInputLbl.innerText = "Preces stāvoklis:";

    fieldset.append(productConditionInputLbl, productConditionInput);

    const productPriceInput = document.createElement("input");
    productPriceInput.type = "text";
    productPriceInput.name = `product-price-${productCount}`;

    const productPriceInputLbl = document.createElement("label");
    productPriceInputLbl.htmlFor = productPriceInput.name;
    productPriceInputLbl.innerText = "Preces cena:";

    const problemDescriptionInput = document.createElement("textarea");
    problemDescriptionInput.name = `problem-description-${productCount}`;

    const problemDescriptionInputLbl = document.createElement("label");
    problemDescriptionInputLbl.htmlFor = problemDescriptionInput.name;
    problemDescriptionInputLbl.innerText = "Problēmas apraksts:";

    fieldset.append(problemDescriptionInputLbl, problemDescriptionInput);

    const userPasswordInput = document.createElement("input");
    userPasswordInput.type = "text";
    userPasswordInput.name = `user-password-${productCount}`;

    const userPasswordInputLbl = document.createElement("label");
    userPasswordInputLbl.htmlFor = userPasswordInput.name;
    userPasswordInputLbl.innerText = "Lietotāja parole:";

    fieldset.append(userPasswordInputLbl, userPasswordInput);

    return fieldset;
}

function additionalInputFields() {
    const fieldset = document.createElement("fieldset");
    fieldset.id = "additional-input-fields-fieldset";

    const legend = document.createElement("legend");
    legend.innerText = "papildus";

    fieldset.append(legend);

    const notesInput = document.createElement("textarea");
    notesInput.name = "notes";

    const notesInputLbl = document.createElement("label");
    notesInputLbl.htmlFor = notesInput.name;
    notesInputLbl.innerText = "piezīmes";

    fieldset.append(notesInputLbl, notesInput);

    const requestedSolutionSelectElement = document.createElement("select");
    requestedSolutionSelectElement.name = "requested-solution";

    const options = VALUES.requestedSolution;
    for (const key in options) {
        if (options.hasOwnProperty(key)) {
            const option = document.createElement("option");
            option.value = key;
            option.textContent = options[key as keyof typeof options];
            requestedSolutionSelectElement.append(option);
        }
    }

    const requestedSolutionLbl = document.createElement("label");
    requestedSolutionLbl.htmlFor = requestedSolutionSelectElement.name;
    requestedSolutionLbl.innerText = "risinājums";

    fieldset.append(requestedSolutionLbl, requestedSolutionSelectElement);

    return fieldset;
}
