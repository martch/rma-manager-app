import {
    QuerySnapshot,
    query,
    collection,
    where,
    orderBy,
    getDocs,
} from "firebase/firestore";
import { FIRESTORE } from "../firebase";

export async function getRmaTblData(filters: object): Promise<QuerySnapshot> {
    console.log(filters);

    const q = query(
        collection(FIRESTORE, "case_data"),
        where("case_status", "==", "active"),
        orderBy("case_id", "desc")
    );

    try {
        const data = await getDocs(q);
        return data;
    } catch (error) {
        console.error(error);
        throw `error: ${error}`;
    }
}
