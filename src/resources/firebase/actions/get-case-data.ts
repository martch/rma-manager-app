import { getDoc, doc } from "firebase/firestore";
import { FIRESTORE } from "../firebase";
import { CaseData } from "../../case-data";

export async function getCaseData(caseId: string) {
    try {
        const document = await getDoc(doc(FIRESTORE, "case_data", caseId));
        if (document.exists()) {
            const dataRaw = document.data();
            const data: CaseData = {
                case_id: dataRaw.case_id,
                client: dataRaw.client,
            };
            return data;
        } else {
            return null;
        }
    } catch (error) {
        console.error(error);
        throw `could not get case data from firestore because: ${error}`;
    }
}
