export interface CaseData {
    case_id: number;
    client: {
        client_company: string;
        client_name: string;
        client_phone: string;
        client_email: string;
        client_order_id: string;
        client_order_date: number;
        client_bank_account: string;
    };
}
