import { logoutBtn } from "./buttons/logout-btn";
import { newCaseFormBtn } from "./buttons/new-case-btn";

export function navbar() {
    const navbar = document.createElement("div");
    navbar.classList.add("navbar");
    navbar.append(logoutBtn(), newCaseFormBtn());

    return navbar;
}
