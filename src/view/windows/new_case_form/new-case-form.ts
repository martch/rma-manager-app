import { submitForm } from "./actions/submit-form";
import { VALUES } from "../../../external_values/external-values";

export function newCaseForm(): HTMLDialogElement {
    const form = document.createElement("form");
    form.id = "new-case-form";
    form.method = "dialog";
    form.autocomplete = "off";

    const clientCompanyInput = document.createElement("input");
    clientCompanyInput.type = "text";
    clientCompanyInput.name = "client-company";
    clientCompanyInput.placeholder = "Firma SIA";
    clientCompanyInput.classList.add("new-case-form-input");

    const clientCompanyLbl = document.createElement("label");
    clientCompanyLbl.htmlFor = clientCompanyInput.name;
    clientCompanyLbl.innerText = "Uzņēmums:";

    form.append(clientCompanyLbl, clientCompanyInput);

    const clientNameInput = document.createElement("input");
    clientNameInput.type = "text";
    clientNameInput.name = "client-name";
    clientNameInput.placeholder = "Vārds Uzvārds";
    clientNameInput.classList.add("new-case-form-input");

    const clientNameLbl = document.createElement("label");
    clientNameLbl.htmlFor = clientNameInput.name;
    clientNameLbl.innerText = "Klients:";

    form.append(clientNameLbl, clientNameInput);

    const clientPhoneInput = document.createElement("input");
    clientPhoneInput.id = "new-case-form-client-phone-input";
    clientPhoneInput.type = "tel";
    clientPhoneInput.name = "client-phone";
    clientPhoneInput.pattern = "[0-9]{8}";
    clientPhoneInput.placeholder = "23456789";
    clientPhoneInput.classList.add("new-case-form-input");

    const clientPhoneLbl = document.createElement("label");
    clientPhoneLbl.htmlFor = clientNameInput.name;
    clientPhoneLbl.innerText = "Telefona numurs: +371";

    form.append(clientPhoneLbl, clientPhoneInput);

    const clientEmailInput = document.createElement("input");
    clientEmailInput.id = "new-case-form-client-email-input";
    clientEmailInput.type = "email";
    clientEmailInput.name = "client-email";
    clientEmailInput.placeholder = "klients@epasts.lv";
    clientEmailInput.classList.add("new-case-form-input");

    const clientEmailLbl = document.createElement("label");
    clientEmailLbl.htmlFor = clientNameInput.name;
    clientEmailLbl.innerText = "Epasts:";

    form.append(clientEmailLbl, clientEmailInput);

    const clientOrderIdInput = document.createElement("input");
    clientOrderIdInput.type = "text";
    clientOrderIdInput.name = "client-order-id";
    clientOrderIdInput.placeholder = "E123456";
    clientOrderIdInput.pattern = "[E]{1}[0-9]{6}";
    clientOrderIdInput.classList.add("new-case-form-input");

    const clientOrderIdLbl = document.createElement("label");
    clientOrderIdLbl.htmlFor = clientNameInput.name;
    clientOrderIdLbl.innerText = "Pasūtījuma numurs:";

    form.append(clientOrderIdLbl, clientOrderIdInput);

    const clientOrderDateInput = document.createElement("input");
    clientOrderDateInput.type = "date";
    clientOrderDateInput.name = "client-order-date";
    clientOrderDateInput.classList.add("new-case-form-input");

    const clientOrderDateLbl = document.createElement("label");
    clientOrderDateLbl.htmlFor = clientNameInput.name;
    clientOrderDateLbl.innerText = "PPR datums:";

    form.append(clientOrderDateLbl, clientOrderDateInput);

    const clientBankAccountInput = document.createElement("input");
    clientBankAccountInput.type = "text";
    clientBankAccountInput.name = "client-bank-account";
    clientBankAccountInput.classList.add("new-case-form-input");

    const clientBankAccountLbl = document.createElement("label");
    clientBankAccountLbl.htmlFor = clientNameInput.name;
    clientBankAccountLbl.innerText = "Bankas konta numurs:";

    form.append(clientBankAccountLbl, clientBankAccountInput);

    const notesTextarea = document.createElement("textarea");
    notesTextarea.name = "notes";
    notesTextarea.classList.add("new-case-form-input");

    const notesTextareaLbl = document.createElement("label");
    notesTextareaLbl.htmlFor = notesTextarea.name;
    notesTextareaLbl.innerText = "Piezīmes:";

    form.append(notesTextareaLbl, notesTextarea);

    const selectRequestedSolution = document.createElement("select");
    selectRequestedSolution.name = "requested-solution";
    selectRequestedSolution.classList.add("new-case-form-input");

    const options = VALUES.requestedSolution;

    for (const key in options) {
        // if (options.hasOwnProperty(key)) {
        const option = document.createElement("option");
        option.value = key;
        option.textContent = options[key as keyof typeof options];
        selectRequestedSolution.append(option);
        // }
    }

    const selectRequestedSolutionLbl = document.createElement("label");
    selectRequestedSolutionLbl.htmlFor = selectRequestedSolution.name;
    selectRequestedSolutionLbl.innerText = "Risinājums:";

    form.append(selectRequestedSolutionLbl, selectRequestedSolution);

    const productCounter = document.createElement("div");
    productCounter.id = "new-case-form-product-counter";
    productCounter.setAttribute("data-number-of-products", "1");

    form.append(productCounter);

    form.append(
        productFieldset(
            Number(productCounter.getAttribute("data-number-of-products"))
        )
    );

    const addProductBtn = document.createElement("button");
    addProductBtn.id = "new-case-form-add-product-btn";
    addProductBtn.type = "button";
    addProductBtn.innerText = "Pievienot preci";
    addProductBtn.addEventListener("click", () => {
        const productCounter = <HTMLDivElement>(
            document.querySelector("#new-case-form-product-counter")
        );
        let productCount = Number(
            productCounter.getAttribute("data-number-of-products")
        );
        if (productCount < 10) {
            productCount++;
            productCounter.setAttribute(
                "data-number-of-products",
                String(productCount)
            );
            (<HTMLFormElement>(
                document.querySelector("#new-case-form")
            )).insertBefore(
                productFieldset(productCount),
                document.querySelector("#new-case-form-add-product-btn")
            );
        }
    });

    form.append(addProductBtn);

    const removeProductBtn = document.createElement("button");
    removeProductBtn.type = "button";
    removeProductBtn.innerText = "Nodzēst preci";
    removeProductBtn.addEventListener("click", () => {
        const productCounter = <HTMLDivElement>(
            document.querySelector("#new-case-form-product-counter")
        );
        let productCount = Number(
            productCounter.getAttribute("data-number-of-products")
        );
        if (productCount > 1) {
            (<HTMLFieldSetElement>(
                document.querySelector("#product-fieldset-" + productCount)
            )).remove();
            productCount--;
            productCounter.setAttribute(
                "data-number-of-products",
                String(productCount)
            );
        }
    });

    form.append(removeProductBtn);

    const closeFormBtn = document.createElement("button");
    closeFormBtn.id = "close-form-btn";
    closeFormBtn.type = "button";
    closeFormBtn.innerHTML = "aizvērt";
    closeFormBtn.addEventListener("click", () => {
        const newCaseDialog = <HTMLDialogElement>(
            document.querySelector("#new-case-dialog")
        );
        newCaseDialog.close();
        (<HTMLButtonElement>(
            document.querySelector("#open-new-case-form-btn")
        )).style.display = "block";
    });

    form.append(closeFormBtn);

    const resetFormBtn = document.createElement("button");
    resetFormBtn.id = "reset-new-case-form-btn";
    resetFormBtn.type = "reset";
    resetFormBtn.innerHTML = "nodzēst";

    form.append(resetFormBtn);

    const submitFormBtn = document.createElement("button");
    submitFormBtn.id = "add-new-case-btn";
    submitFormBtn.type = "button";
    submitFormBtn.innerHTML = "pievienot";
    submitFormBtn.addEventListener("click", async () => {
        let isFormValid = true;

        const clientPhoneInput = <HTMLInputElement>(
            document.querySelector("#new-case-form-client-phone-input")
        );

        if (clientPhoneInput.validity.patternMismatch) {
            clientPhoneInput.setCustomValidity("nepareizi noformēts tel. nr.");
            clientPhoneInput.reportValidity();
            isFormValid = false;
        } else {
            clientPhoneInput.setCustomValidity("");
        }

        const clientEmailInput = <HTMLInputElement>(
            document.querySelector("#new-case-form-client-email-input")
        );

        if (clientEmailInput.validity.typeMismatch) {
            clientEmailInput.setCustomValidity(
                "nepareizi noformēta epasta adrese"
            );
            clientEmailInput.reportValidity();
            isFormValid = false;
        } else {
            clientEmailInput.setCustomValidity("");
        }

        const itemNameTextarea = <HTMLTextAreaElement>(
            document.querySelector(".new-case-form-product-name-textarea")
        );

        if (itemNameTextarea.validity.valueMissing) {
            itemNameTextarea.setCustomValidity("jānorāda preces nosaukums");
            itemNameTextarea.reportValidity();
            isFormValid = false;
        } else {
            itemNameTextarea.setCustomValidity("");
        }

        if (isFormValid) {
            await submitForm();
            (<HTMLFormElement>document.querySelector("#new-case-form")).reset();
            (<HTMLDialogElement>(
                document.querySelector("#new-case-dialog")
            )).close();
            (<HTMLButtonElement>(
                document.querySelector("#open-new-case-form-btn")
            )).style.display = "block";
        }
    });

    form.append(submitFormBtn);

    const dialog = document.createElement("dialog");
    dialog.id = "new-case-dialog";

    dialog.append(form);

    return dialog;
}

function productFieldset(productCount: number) {
    const productFieldset = document.createElement("fieldset");
    productFieldset.id = `product-fieldset-${productCount}`;

    const productFieldsetLegend = document.createElement("legend");
    productFieldsetLegend.innerText = `Prece ${productCount}:`;

    productFieldset.append(productFieldsetLegend);

    const productNameTextarea = document.createElement("textarea");
    productNameTextarea.classList.add(
        "new-case-form-input",
        "new-case-form-product-name-textarea"
    );
    productNameTextarea.name = `product-name-${productCount}`;
    productNameTextarea.required = true;

    const productNameTextareaLbl = document.createElement("label");
    productNameTextareaLbl.htmlFor = productNameTextarea.name;
    productNameTextareaLbl.innerText = "Preces nosaukums:";

    productFieldset.append(productNameTextareaLbl, productNameTextarea);

    const productSnInput = document.createElement("input");
    productSnInput.type = "text";
    productSnInput.name = `product-sn-${productCount}`;
    productSnInput.classList.add("new-case-form-input");

    const productSnInputLbl = document.createElement("label");
    productSnInputLbl.htmlFor = productSnInput.name;
    productSnInputLbl.innerText = "Sērijas numurs:";

    productFieldset.append(productSnInputLbl, productSnInput);

    const productSetTextarea = document.createElement("textarea");
    productSetTextarea.name = `product-set-${productCount}`;
    productSetTextarea.classList.add("new-case-form-input");

    const productSetTextareaLbl = document.createElement("label");
    productSetTextareaLbl.htmlFor = productSetTextarea.name;
    productSetTextareaLbl.innerText = "Komplektācija:";

    productFieldset.append(productSetTextareaLbl, productSetTextarea);

    const productConditionTextarea = document.createElement("textarea");
    productConditionTextarea.name = `product-condition-${productCount}`;
    productConditionTextarea.classList.add("new-case-form-input");

    const productConditionTextareaLbl = document.createElement("label");
    productConditionTextareaLbl.htmlFor = productConditionTextarea.name;
    productConditionTextareaLbl.innerText = "Preces stāvoklis:";

    productFieldset.append(
        productConditionTextareaLbl,
        productConditionTextarea
    );

    const productPriceInput = document.createElement("input");
    productPriceInput.type = "text";
    productPriceInput.name = `product-price-${productCount}`;
    productPriceInput.classList.add("new-case-form-input");

    const productPriceInputLbl = document.createElement("label");
    productPriceInputLbl.htmlFor = productPriceInput.name;
    productPriceInputLbl.innerText = "Preces cena:";

    productFieldset.append(productPriceInputLbl, productPriceInput);

    const problemDescriptionTextarea = document.createElement("textarea");
    problemDescriptionTextarea.name = `problem-description-${productCount}`;
    problemDescriptionTextarea.classList.add("new-case-form-input");

    const problemDescriptionTextareaLbl = document.createElement("label");
    problemDescriptionTextareaLbl.htmlFor = problemDescriptionTextarea.name;
    problemDescriptionTextareaLbl.innerText = "Problēmas apraksts:";

    productFieldset.append(
        problemDescriptionTextareaLbl,
        problemDescriptionTextarea
    );

    const userPasswordInput = document.createElement("input");
    userPasswordInput.type = "text";
    userPasswordInput.name = `user-password-${productCount}`;
    userPasswordInput.classList.add("new-case-form-input");
    userPasswordInput.required = true;

    const userPasswordInputLbl = document.createElement("label");
    userPasswordInputLbl.htmlFor = userPasswordInput.name;
    userPasswordInputLbl.innerText = "Lietotāja parole:";

    productFieldset.append(userPasswordInputLbl, userPasswordInput);

    return productFieldset;
}
