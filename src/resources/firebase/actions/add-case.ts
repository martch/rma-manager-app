import { doc, setDoc } from "firebase/firestore";
import { FIRESTORE } from "../firebase";
import { CaseData } from "../../case-data";

export async function addCase(formData: CaseData) {
    console.log(formData);
    await setDoc(
        doc(FIRESTORE, "case_data", String(formData.case_id)),
        formData
    );
}
