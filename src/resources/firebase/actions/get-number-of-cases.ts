import { getCountFromServer, collection } from "firebase/firestore";
import { FIRESTORE } from "../firebase";

export async function getNumberOfCases(): Promise<number> {
    try {
        const snapshot = await getCountFromServer(
            collection(FIRESTORE, "case_data")
        );
        return snapshot.data().count;
    } catch (error) {
        console.error(error);
        throw `error: ${error}`;
    }
}
