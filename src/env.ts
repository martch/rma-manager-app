export const ENV = {
    isDevelopment: false,
    get development() {
        return this.isDevelopment;
    },
    set development(value) {
        this.isDevelopment = value;
    },
};
