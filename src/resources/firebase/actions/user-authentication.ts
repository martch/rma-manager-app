import { signInWithEmailAndPassword, signOut } from "firebase/auth";
import { AUTH } from "../firebase";

export async function login(email: string, password: string) {
    try {
        await signInWithEmailAndPassword(AUTH, email, password);
    } catch (error) {
        throw `error: ${error}`;
    }
}

export async function logout() {
    try {
        await signOut(AUTH);
    } catch (error) {
        throw `error: ${error}`;
    }
}
