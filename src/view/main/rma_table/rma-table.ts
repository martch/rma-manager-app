import { getRmaTblData } from "../../../resources/firebase/actions/get-rma-tbl-data";
import { openCaseForm } from "./actions/open-case-form";

export async function rmaTable(filters: object): Promise<HTMLTableElement> {
    const tblHeaderRow = document.createElement("tr");
    tblHeaderRow.append(
        tblHeaderCell("case id"),
        tblHeaderCell("klienta statuss"),
        tblHeaderCell("servisa statuss"),
        tblHeaderCell("klients"),
        tblHeaderCell("uzņēmums"),
        tblHeaderCell("prece"),
        tblHeaderCell("sērijas numurs"),
        tblHeaderCell("pievienots"),
        tblHeaderCell("mainīts")
    );

    const rmaTbl = document.createElement("table");
    rmaTbl.id = "rma-table";
    rmaTbl.append(tblHeaderRow);

    try {
        const data = await getRmaTblData(filters);

        if (data == null) {
            console.warn("no data");
            return rmaTbl;
        }

        data.forEach((doc) => {
            const data = doc.data();
            let productNames = "";
            let productSerialNumbers = "";
            if (data.products.size > 0) {
                const numberOfProducts = Object.keys(data.products).length;
                if (numberOfProducts > 1) {
                    for (let i = 0; i < numberOfProducts; i++) {
                        if (i > 0) {
                            productNames += " // ";
                            productSerialNumbers += " // ";
                        }
                        productNames +=
                            data.products["product_" + (i + 1)].product_name;
                        productSerialNumbers +=
                            data.products["product_" + (i + 1)].product_sn;
                    }
                } else {
                    productNames = data.products["product_1"].product_name;
                    productSerialNumbers =
                        data.products["product_1"].product_sn;
                }
            }

            const caseIdCell = tblDataCell();

            const caseIdBtn = document.createElement("button");
            caseIdBtn.id = `open-case-form-btn-${data.case_id}`;
            caseIdBtn.type = "button";
            caseIdBtn.innerHTML = data.case_id;
            caseIdBtn.addEventListener("click", () => {
                openCaseForm(data.case_id);
            });

            caseIdCell.append(caseIdBtn);

            const tblDataRow = document.createElement("tr");
            tblDataRow.append(
                caseIdCell,
                tblDataCell("n/a"),
                tblDataCell("n/a"),
                tblDataCell(data.client.client_name),
                tblDataCell(data.client.client_company),
                tblDataCell(productNames),
                tblDataCell(productSerialNumbers),
                tblDataCell(data.case_added_timestamp),
                tblDataCell(data.last_change_timestamp)
            );
            rmaTbl.append(tblDataRow);
        });
        return rmaTbl;
    } catch (error) {
        console.error(error);
        return rmaTbl;
    }
}

function tblHeaderCell(textContent: string) {
    const cell = document.createElement("th");
    cell.textContent = textContent;
    return cell;
}

function tblDataCell(textContent?: string) {
    const cell = document.createElement("td");
    if (textContent) {
        cell.textContent = textContent;
    }
    return cell;
}
