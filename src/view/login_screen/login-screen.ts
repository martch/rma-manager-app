import { submitLoginForm } from "./actions/submit-login-form";

export async function openLoginScreen() {
    const loginForm = document.createElement("form");
    loginForm.id = "login-form";

    const emailInputField = document.createElement("input");
    emailInputField.type = "email";
    emailInputField.name = "email";
    emailInputField.autocomplete = "username";

    const emailInputFieldLbl = document.createElement("label");
    emailInputFieldLbl.htmlFor = emailInputField.name;

    loginForm.append(emailInputFieldLbl, emailInputField);

    const passwordInputField = document.createElement("input");
    passwordInputField.type = "password";
    passwordInputField.name = "password";
    passwordInputField.autocomplete = "password";

    const passwordInputFieldLbl = document.createElement("label");
    passwordInputFieldLbl.htmlFor = passwordInputField.name;

    loginForm.append(passwordInputFieldLbl, passwordInputField);

    const loginButton = document.createElement("button");
    loginButton.id = "login-submit-btn";
    loginButton.type = "submit";
    loginButton.innerText = "pierakstīties";

    loginForm.append(loginButton);

    loginForm.addEventListener("submit", async (event) => {
        submitLoginForm(event);
    });

    const body = document.querySelector("#body");
    if (body == null) {
        throw "could not find the body";
    }
    body.replaceChildren(loginForm);
}
