import { FIREBASE_CONFIG } from "./config";
import { ENV } from "../../env";
import { loadApp } from "../../load-app";
import { initializeApp } from "firebase/app";
import {
    getAuth,
    onAuthStateChanged,
    connectAuthEmulator,
} from "firebase/auth";
import { getFirestore, connectFirestoreEmulator } from "firebase/firestore";
import { openLoginScreen } from "../../view/login_screen/login-screen";

const firebaseApp = initializeApp(FIREBASE_CONFIG);
export const FIRESTORE = getFirestore(firebaseApp);
export const AUTH = getAuth(firebaseApp);

export function initFirebase() {
    if (location.hostname === "localhost") {
        try {
            ENV.development = true;
            connectFirestoreEmulator(FIRESTORE, "localhost", 8080);
            connectAuthEmulator(AUTH, "http://localhost:9099");
        } catch (error) {
            console.error(error);
            throw `error: ${error}`;
        }
    }

    onAuthStateChanged(AUTH, async (user) => {
        if (user == null) {
            try {
                await openLoginScreen();
            } catch (error) {
                console.error(error);
                throw `error: ${error}`;
            }
        } else {
            sessionStorage.setItem("user", JSON.stringify(user));
            try {
                await loadApp();
            } catch (error) {
                console.error(error);
                throw `error: ${error}`;
            }
        }
    });
}
