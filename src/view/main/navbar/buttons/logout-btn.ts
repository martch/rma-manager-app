import { logout } from "../../../../resources/firebase/actions/user-authentication";

export function logoutBtn() {
    const button = document.createElement("button");
    button.type = "button";
    button.innerText = "izrakstīties";
    button.addEventListener("click", onClickLogout);

    return button;
}

async function onClickLogout() {
    try {
        await logout();
    } catch (error) {
        console.error(error);
        throw `error: ${error}`;
    }
}
