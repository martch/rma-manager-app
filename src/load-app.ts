import { getExternalValues } from "./external_values/external-values";
import { rmaTable } from "./view/main/rma_table/rma-table";
import { navbar } from "./view/main/navbar/navbar";
import { filters } from "./view/main/filters/filters";

export async function loadApp() {
    try {
        await getExternalValues();
    } catch (error) {
        console.error(error);
    }

    const body = document.querySelector("#body");
    if (body == null) {
        console.warn("missing body");
        return;
    }

    body.replaceChildren();
    body.append(layoutNavbar(), await layoutRmaTable(), layoutFilters());
}

function layoutNavbar() {
    const navbarWrapper = document.createElement("div");
    navbarWrapper.style.border = "2px dotted red";
    navbarWrapper.append(navbar());
    return navbarWrapper;
}

async function layoutRmaTable() {
    const rmaTableWrapper = document.createElement("div");
    rmaTableWrapper.style.border = "2px dotted green";
    rmaTableWrapper.append(await rmaTable({ case_status: "active" }));
    return rmaTableWrapper;
}

function layoutFilters() {
    const filtersWrapper = document.createElement("div");
    filtersWrapper.style.border = "2px dotted blue";
    filtersWrapper.append(filters());
    return filtersWrapper;
}
