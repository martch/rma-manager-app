import { newCaseForm } from "../../../windows/new_case_form/new-case-form";

export function newCaseFormBtn() {
    const button = document.createElement("button");
    button.id = "open-new-case-form-btn";
    button.type = "button";
    button.innerText = "jauns RMA";
    button.addEventListener("click", onClickNewCaseForm);

    return button;
}

function onClickNewCaseForm() {
    let newCaseDialog: HTMLDialogElement | null =
        document.querySelector("#new-case-dialog");

    if (newCaseDialog == null) {
        const body = document.querySelector("#body");
        if (body == null) {
            throw "missing body";
        }

        newCaseDialog = newCaseForm();

        body.append(newCaseDialog);
    }

    newCaseDialog.show();

    const openNewCaseFormButton: HTMLButtonElement | null =
        document.querySelector("#open-new-case-form-btn");
    if (openNewCaseFormButton == null) {
        throw "missing open new case form button";
    }

    openNewCaseFormButton.style.display = "none";
}
