import { addCase } from "../../../../resources/firebase/actions/add-case";
import { getNumberOfCases } from "../../../../resources/firebase/actions/get-number-of-cases";

export async function submitForm() {
    const numberOfCases: number = (await getNumberOfCases()) + 1;
    const newCaseForm = <HTMLFormElement>(
        document.querySelector("#new-case-form")
    );
    if (newCaseForm === null) {
        console.warn("could not find #new-case-form");
        return;
    }
    const productCounter = document.querySelector(
        "#new-case-form-product-counter"
    );
    let productCount = 0;
    if (productCounter !== null) {
        productCount = Number(
            productCounter.getAttribute("data-number-of-products")
        );
    }
    const productData = {} as any;
    if (productCount > 0) {
        for (let i = 1; i <= productCount; i++) {
            productData[i] = {
                product_name: newCaseForm["product-name-" + i].value,
                product_sn: newCaseForm["product-sn-" + i].value,
                product_set: newCaseForm["product-set-" + i].value,
                product_condition: newCaseForm["product-condition-" + i].value,
                product_price: newCaseForm["product-price-" + i].value,
                problem_description:
                    newCaseForm["problem-description-" + i].value,
                user_password: newCaseForm["user-password-" + i].value,
            };
        }
    }
    const data = {
        case_added_timestamp: new Date().getTime(),
        case_id: numberOfCases,
        case_status: "active",
        client: {
            client_name: newCaseForm["client-name"].value,
            client_company: newCaseForm["client-company"].value,
            client_phone: newCaseForm["client-phone"].value,
            client_email: newCaseForm["client-email"].value,
            client_order_id: newCaseForm["client-order-id"].value,
            client_order_date: new Date(
                newCaseForm["client-order-date"].value
            ).getTime(),
            client_bank_account: newCaseForm["client-bank-account"].value,
        },
        last_change_timestamp: new Date().getTime(),
        products: productData,
    };
    try {
        await addCase(data);
    } catch (error) {
        console.error(error);
    }
}
