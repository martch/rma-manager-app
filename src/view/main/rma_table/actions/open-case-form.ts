import { caseForm } from "../../../windows/case_form/case_form";

export async function openCaseForm(caseId: string) {
    try {
        const openCaseForm = await caseForm(caseId);

        const body = document.querySelector("#body");
        if (body == null) {
            console.warn("missing body");
            return;
        }

        body.append(openCaseForm);

        openCaseForm.show();
    } catch (error) {
        console.error(error);
    }
}
