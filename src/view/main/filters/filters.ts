import { rmaTable } from "../rma_table/rma-table";

export function filters() {
    const filters = document.createElement("form");

    const caseIdInputField = document.createElement("input");
    caseIdInputField.id = "filters-case-id";
    caseIdInputField.type = "text";
    caseIdInputField.name = "case-id";

    const caseIdInputFieldLbl = document.createElement("label");
    caseIdInputFieldLbl.htmlFor = caseIdInputField.name;

    filters.append(caseIdInputFieldLbl, caseIdInputField);

    const applyFiltersButton = document.createElement("button");
    applyFiltersButton.id = "apply-filters-btn";
    applyFiltersButton.type = "button";
    applyFiltersButton.innerText = "atlasīt";
    applyFiltersButton.addEventListener("click", applyFilters);

    filters.append(applyFiltersButton);

    return filters;
}

async function applyFilters() {
    const rmaTbl = document.querySelector("#rma-table");

    if (rmaTbl == null) {
        throw "missing rma table";
    }

    const filtersCaseId: HTMLInputElement | null =
        document.querySelector("#filters-case-id");

    if (filtersCaseId == null) {
        throw "filter is missing case id field";
    }

    try {
        const newTbl = await rmaTable({
            case_status: "active",
            case_id: Number(filtersCaseId.value),
        });

        rmaTbl.replaceWith(newTbl);
    } catch (error) {
        console.error(error);
        throw `error: ${error}`;
    }
}
