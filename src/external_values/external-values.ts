import {
    doc,
    getDoc,
    // } from "https://www.gstatic.com/firebasejs/9.22.2/firebase-firestore.js";
} from "firebase/firestore";
import { FIRESTORE } from "../resources/firebase/firebase";
import { DocumentSnapshot } from "firebase/firestore";

export const VALUES = {
    clientStatusValues: {},
    get clientStatus() {
        return this.clientStatusValues;
    },
    set clientStatus(value: object) {
        this.clientStatusValues = value;
    },
    serviceStatusValues: {},
    get serviceStatus() {
        return this.serviceStatusValues;
    },
    set serviceStatus(value: object) {
        this.serviceStatusValues = value;
    },
    suppliersValues: {},
    get suppliers() {
        return this.suppliersValues;
    },
    set suppliers(value: object) {
        this.suppliersValues = value;
    },
    serviceProvidersValues: {},
    get serviceProviders() {
        return this.serviceStatusValues;
    },
    set serviceProviders(value: object) {
        this.serviceStatusValues = value;
    },
    requestedSolutionValues: {},
    get requestedSolution() {
        return this.requestedSolutionValues;
    },
    set requestedSolution(value: object) {
        this.requestedSolutionValues = value;
    },
};

// TODO: sort values alphabetically by key
export async function getExternalValues() {
    try {
        const clientStatusDoc = await getDoc(
            doc(FIRESTORE, "values", "client_status")
        );
        if (clientStatusDoc.exists()) {
            VALUES.clientStatus = clientStatusDoc.data();
        }
        const serviceStatusDoc = await getDoc(
            doc(FIRESTORE, "values", "service_status")
        );
        if (serviceStatusDoc.exists()) {
            VALUES.serviceStatus = serviceStatusDoc.data();
        }
        const suppliersDoc = await getDoc(
            doc(FIRESTORE, "values", "suppliers")
        );
        if (suppliersDoc.exists()) {
            VALUES.suppliers = suppliersDoc.data();
        }
        const serviceProvidersDoc = await getDoc(
            doc(FIRESTORE, "values", "service_providers")
        );
        if (serviceProvidersDoc.exists()) {
            VALUES.serviceProviders = serviceProvidersDoc.data();
        }
        const requestedSolutionDoc: DocumentSnapshot = await getDoc(
            doc(FIRESTORE, "values", "requested_solution")
        );
        if (requestedSolutionDoc.exists()) {
            // const unordered = requestedSolutionDoc.data();
            // const ordered = Object.keys(unordered)
            //     .sort()
            //     .reduce((obj, key) => {
            //         obj[key] = unordered[key];
            //         return obj;
            //     }, {});
            // VALUES.requestedSolution = ordered;
            VALUES.requestedSolution = requestedSolutionDoc;
        }
    } catch (error) {
        console.error(error);
    }
}
