import { initFirebase } from "./resources/firebase/firebase";

try {
    main();
} catch (error) {
    console.error(error);
}

async function main() {
    try {
        initFirebase();
    } catch (error) {
        console.error(error);
    }
}
